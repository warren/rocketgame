package warrenfalk.rocketgame;

import java.io.IOException;
import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

import warrenfalk.gl.Mesh;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import static warrenfalk.gl.Util.*;

public class Main {
	static Mesh mesh;
	static int shaders;
	static int paramProjectionMatrix;
	static int paramViewMatrix;
	static int paramModelMatrix;
	static Matrix4f viewMatrix = new Matrix4f();
	static Matrix4f modelMatrix = new Matrix4f();
	static Matrix4f projectionMatrix = new Matrix4f();
	
	static Vector3f cameraPos = new Vector3f();
	
	static Vector3f modelPos = new Vector3f();
	static float modelAttitude;
	static float modelAngVel;
	static Vector3f modelVelocity = new Vector3f();
	static Vector3f modelRotvel = new Vector3f();
	
	static boolean keyLeftFlag;
	static boolean keyLeftStatus;
	static boolean keyRightFlag;
	static boolean keyRightStatus;
	
	static long thisTick;
	static long lastTick;
	
	public static void main(String[] args) throws Exception {
		initOpenGl();
		initMeshes();
		initShaders();
		
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		Display.update();

		FloatBuffer mb = BufferUtils.createFloatBuffer(16);
		
		createProjectionMatrix(projectionMatrix, 60f, 1024f/768f, 0.1f, 1000f);

		lastTick = System.nanoTime();
		
		// set initial velocity
		//modelVelocity.y = 0.018f;
		
		modelAttitude = 0.0f;
		
		Vector3f viewPos = new Vector3f();
		while (!Display.isCloseRequested()) {
			thisTick = System.nanoTime();
			float timeDelta = (float)((thisTick - lastTick) / 1000) / 1000f;
			
			keyLeftFlag = keyLeftStatus;
			keyRightFlag = keyRightStatus;
			while (Keyboard.next()) {
				switch (Keyboard.getEventKey()) {
					case Keyboard.KEY_LEFT:
						keyLeftStatus = Keyboard.getEventKeyState();
						if (keyLeftStatus)
							keyLeftFlag = true;
						break;
					case Keyboard.KEY_RIGHT:
						keyRightStatus = Keyboard.getEventKeyState();
						if (keyRightStatus)
							keyRightFlag = true;
						break;
				}
			}
			float rocketForce = 0.000003f * timeDelta;
			float thrust = 0f;
			float torque = 0f;
			if (keyLeftFlag) {
				thrust += rocketForce;
				torque -= 0.00003 * timeDelta;
			}
			if (keyRightFlag) {
				thrust += rocketForce;
				torque += 0.00003 * timeDelta;
			}
			
			// do matrices
			modelMatrix.setIdentity();
			// apply rocket force
			modelAngVel += torque;
			modelAttitude += modelAngVel;
			modelVelocity.y += thrust * Math.cos(modelAttitude);
			modelVelocity.x += -thrust * Math.sin(modelAttitude);
			// apply gravity
			//modelVelocity.y += -0.00001f * timeDelta;
			modelPos.translate(modelVelocity.x * timeDelta, modelVelocity.y * timeDelta, modelVelocity.z * timeDelta);
			modelMatrix.translate(modelPos);
			modelMatrix.rotate(modelAttitude, new Vector3f(0f, 0f, 1f));
			// rotate the model based on the attitude by subtracting rotations
			Vector3f normal = new Vector3f(0f, 1f, 0f);
			
			
			viewMatrix.setIdentity();
			Matrix4f.translate(new Vector3f(0, -1.5f, -30f), viewMatrix, viewMatrix);
			cameraPos.set(modelPos.x, modelPos.y);
			cameraPos.negate(viewPos);
			Matrix4f.translate(viewPos, viewMatrix, viewMatrix);
			//Matrix4f.rotate(degtorad(40), new Vector3f(0, 1f, 1f), modelMatrix, modelMatrix);
			
			glUseProgram(shaders);
			projectionMatrix.store(mb);
			mb.flip();
			glUniformMatrix4(paramProjectionMatrix, false, mb);
			viewMatrix.store(mb);
			mb.flip();
			glUniformMatrix4(paramViewMatrix, false, mb);
			modelMatrix.store(mb);
			mb.flip();
			glUniformMatrix4(paramModelMatrix, false, mb);
			
			// do render
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			
			mesh.draw();
			
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
			glDisableVertexAttribArray(0);
			glBindVertexArray(0);
			glUseProgram(0);
			
			Display.sync(60);
			Display.update();
			lastTick = thisTick;
		}
		
		mesh.delete();
		
		Display.destroy();
	}
	
	private static void initShaders() throws IOException {
		int vshader = loadShader("/warrenfalk/gl/vertex.glsl", GL_VERTEX_SHADER);
		int fshader = loadShader("/warrenfalk/gl/fragment.glsl", GL_FRAGMENT_SHADER);
		shaders = glCreateProgram();
		glAttachShader(shaders, vshader);
		glAttachShader(shaders, fshader);
		glLinkProgram(shaders);
		
		glBindAttribLocation(shaders, 0, "in_Position");
		glBindAttribLocation(shaders, 1, "in_Color");
		glBindAttribLocation(shaders, 2, "in_TextureCoord");
		
		paramProjectionMatrix = glGetUniformLocation(shaders, "projectionMatrix");
		paramViewMatrix = glGetUniformLocation(shaders, "viewMatrix");
		paramModelMatrix = glGetUniformLocation(shaders, "modelMatrix");
		
		glValidateProgram(shaders);
	}
	
	private static void initMeshes() throws IOException {
		mesh = Mesh.load("/warrenfalk/rocket.obj");
	}

	private static void initOpenGl() throws LWJGLException {
		PixelFormat pixelFormat = new PixelFormat()
			.withSamples(4);
		ContextAttribs contextAttributes = new ContextAttribs(3, 2)
			.withProfileCore(true)
			.withForwardCompatible(true);
		Display.setDisplayMode(new DisplayMode(1024, 768));
		Display.setTitle("Alpha");
		Display.create(pixelFormat, contextAttributes);
		
		// Setup background color
		glClearColor(0.75f, 0.75f, 0.75f, 0f);
		// Map the internal OpenGL coordinate system to the entire screen
		glViewport(0, 0, 1024, 768);

	}
	

}
