package warrenfalk.gl;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_SHORT;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;

import org.lwjgl.BufferUtils;

public class Mesh {
	final int vertexArrayId;
	final int elementArrayBufferId;
	final int size;
	final int elementType;
	
	public Mesh(int size, int elementType, int vertexArrayId, int elementArrayBufferId) {
		this.size = size;
		this.elementType = elementType;
		this.vertexArrayId = vertexArrayId;
		this.elementArrayBufferId = elementArrayBufferId;
	}
	
	public int size() {
		return size;
	}

	public void delete() {
		glDeleteVertexArrays(vertexArrayId);
	}
	
	public static class Vector {
		float x;
		float y;
		float z;
	}
	
	public static class Coord {
		float u;
		float v;
	}
	
	public static class FaceVertex {
		float x;
		float y;
		float z;
		float nx;
		float ny;
		float nz;
		float u;
		float v;
		
		public FaceVertex(Vector vertex, Vector normal, Coord tex) {
			x = vertex.x;
			y = vertex.y;
			z = vertex.z;
			nx = normal.x;
			ny = normal.y;
			nz = normal.z;
			u = tex.u;
			v = tex.v;
		}
		
		@Override
		public boolean equals(Object obj) {
			return equals((FaceVertex)obj);
		}
		
		public boolean equals(FaceVertex other) {
			return other.x == x && other.y == y && other.z == z && other.nx == nx && other.ny == ny && other.nz == nz && other.u == u && other.v == v;
		}
		
		@Override
		public int hashCode() {
			return Float.floatToIntBits(x) ^ Float.floatToIntBits(y) ^ Float.floatToIntBits(z) ^ Float.floatToIntBits(nx) ^ Float.floatToIntBits(ny) ^ Float.floatToIntBits(nz) ^ Float.floatToIntBits(u) ^ Float.floatToIntBits(v);
		}
	}
	
	public static class Face {
		int fv1;
		int fv2;
		int fv3;
	}
	
	/*
	 * This clumsy function loads a wavefront .obj file into an opengl format.
	 * This is clumsy because in wavefront format, a face vertex can have a separate vertex, normal, and texture index whereas in opengl, it must be one index to a unique combination of the three
	 */
	public static Mesh load(String resource) throws IOException {
		ArrayList<Vector> vertexList = new ArrayList<Vector>();
		ArrayList<Vector> normalsList = new ArrayList<Vector>();
		ArrayList<Coord> texcoordList = new ArrayList<Coord>();
		HashMap<FaceVertex,Integer> faceVertexMap = new HashMap<FaceVertex,Integer>();
		ArrayList<FaceVertex> faceVertexList = new ArrayList<FaceVertex>();
		ArrayList<Face> faceList = new ArrayList<Face>();
		
		InputStream stream = Util.class.getResourceAsStream(resource);
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));;
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.length() == 0)
					continue;
				if (line.startsWith("#"))
					continue;
				String[] parts = line.split(" ");
				if ("v".equals(parts[0])) {
					Vector v = new Vector();
					v.x = Float.parseFloat(parts[1]);
					v.y = Float.parseFloat(parts[2]);
					v.z = Float.parseFloat(parts[3]);
					vertexList.add(v);
				}
				else if ("vt".equals(parts[0])) {
					Coord c = new Coord();
					c.u = Float.parseFloat(parts[1]);
					c.v = Float.parseFloat(parts[2]);
					texcoordList.add(c);
				}
				else if ("vn".equals(parts[0])) {
					Vector vn = new Vector();
					vn.x = Float.parseFloat(parts[1]);
					vn.y = Float.parseFloat(parts[2]);
					vn.z = Float.parseFloat(parts[3]);
					normalsList.add(vn);
				}
				else if ("f".equals(parts[0])) {
					Face face = new Face();
					face.fv1 = getFaceVertexId(vertexList, normalsList, texcoordList, faceVertexMap, faceVertexList, parts[1]);
					face.fv2 = getFaceVertexId(vertexList, normalsList, texcoordList, faceVertexMap, faceVertexList, parts[2]);
					face.fv3 = getFaceVertexId(vertexList, normalsList, texcoordList, faceVertexMap, faceVertexList, parts[3]);
					faceList.add(face);
				}
				else if ("mtllib".equals(parts[0])) {
					// TODO: handle mtllib
				}
				else if ("o".equals(parts[0])) {
					// TODO: handle o
				}
				else if ("usemtl".equals(parts[0])) {
					// TODO: handle usemtl
				}
				else if ("s".equals(parts[0])) {
					// TODO: handle s
				}
				else {
					System.out.println("Unknown object parameter type: " + line);
				}
			}
		}
		finally {
			reader.close();
		}

		FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(8 * faceVertexList.size());
		for (FaceVertex fv : faceVertexList) {
			vertexBuffer.put(fv.x);
			vertexBuffer.put(fv.y);
			vertexBuffer.put(fv.z);
			vertexBuffer.put(fv.nx);
			vertexBuffer.put(fv.ny);
			vertexBuffer.put(fv.nz);
			vertexBuffer.put(fv.u);
			vertexBuffer.put(fv.v);
		}
		vertexBuffer.flip();		

		int elementType = GL_UNSIGNED_SHORT;
		ShortBuffer indicesBuffer = BufferUtils.createShortBuffer(faceList.size() * 3);
		for (Face face : faceList) {
			indicesBuffer.put((short)face.fv1);
			indicesBuffer.put((short)face.fv2);
			indicesBuffer.put((short)face.fv3);
		}
		indicesBuffer.flip();
		
		int vertexArrayId = glGenVertexArrays();
		glBindVertexArray(vertexArrayId);
		int vboId = glGenBuffers();
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, 32, 0);
		glVertexAttribPointer(1, 3, GL_FLOAT, false, 32, 32);
		glVertexAttribPointer(2, 2, GL_FLOAT, false, 32, 64);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		
		int elementArrayBufferId = glGenBuffers();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementArrayBufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		return new Mesh(faceList.size() * 3, elementType, vertexArrayId, elementArrayBufferId);

	}

	private static int getFaceVertexId(ArrayList<Vector> vertexList, ArrayList<Vector> normalsList, ArrayList<Coord> texcoordList, HashMap<FaceVertex, Integer> faceVertexMap, ArrayList<FaceVertex> faceVertexList, String decl) {
		String[] subParts = decl.split("/");
		int v = Integer.parseInt(subParts[0]) - 1;
		int vt = Integer.parseInt(subParts[1]) - 1;
		int vn = Integer.parseInt(subParts[2]) - 1;
		FaceVertex fvd = new FaceVertex(vertexList.get(v), normalsList.get(vn), texcoordList.get(vt));
		Integer index = faceVertexMap.get(fvd);
		if (index == null) {
			index = faceVertexList.size();
			faceVertexList.add(fvd);
			faceVertexMap.put(fvd, index);
		}
		return index;
	}

	public void draw() {
		glBindVertexArray(vertexArrayId);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementArrayBufferId);
		glDrawElements(GL_TRIANGLES, size(), elementType, 0);
	}

}
