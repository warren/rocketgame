package warrenfalk.gl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.lwjgl.util.vector.Matrix4f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;


public class Util {
	public static final double PI = 3.14159265358979323846;

	public static float cotan(float angle) {
		return (float)(1f / Math.tan(angle));
	}
	
	public static float degtorad(float degrees) {
		return degrees * (float)(PI / 180d);
	}
	
	public static Matrix4f createProjectionMatrix(float fov, float aspect, float near, float far) {
		Matrix4f m = new Matrix4f();
		createProjectionMatrix(m, fov, aspect, near, far);
		return m;
	}
	
	public static void createProjectionMatrix(Matrix4f m, float fov, float aspect, float near, float far) {
		m.setIdentity();
		float y_scale = cotan(degtorad(fov / 2f));
		float x_scale = y_scale / aspect;
		float frustum_length = far - near;
		m.m00 = x_scale;
		m.m11 = y_scale;
		m.m22 = -((far + near) / frustum_length);
		m.m23 = -1;
		m.m32 = -((2 * near * far) / frustum_length);
		m.m33 = 0;
	}

	public static int loadShader(String resource, int type) throws IOException {
		StringBuilder shaderSource = new StringBuilder();
		int shaderID = 0;

		InputStream stream = Util.class.getResourceAsStream(resource);
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));;
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				shaderSource.append(line).append("\n");
			}
		}
		finally {
			reader.close();
		}
		
		shaderID = glCreateShader(type);
		glShaderSource(shaderID, shaderSource);
		glCompileShader(shaderID);
		
		if (glGetShaderi(shaderID, GL_COMPILE_STATUS) == GL_FALSE) {
			System.err.println(glGetShaderInfoLog(shaderID, 4096));
			System.err.println("Could not compile shader.");
			System.exit(-1);
		}
		
		return shaderID;
	}
}
