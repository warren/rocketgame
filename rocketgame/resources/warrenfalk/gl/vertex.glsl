#version 330
in vec4 vVertex;
in vec3 vNormal;
//uniform mat4 mvpMatrix;  // mvp = ModelViewProjection
//uniform mat4 mvMatrix; // mv = ModelView
//uniform mat3 normalMatrix;
//uniform vec3 vLightPosition;
smooth out vec3 vVaryingNormal;
smooth out vec3 vVaryingLightDir;


uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

//in vec4 in_Position;
//in vec4 in_Color;
//in vec2 in_TextureCoord;

//out vec4 pass_Color;
//out vec2 pass_TextureCoord;


void main(void) {
 mat4 mvpMatrix = projectionMatrix * viewMatrix * modelMatrix;
 mat4 mvMatrix = viewMatrix * modelMatrix;
 mat3 normalMatrix;
 normalMatrix[0] = vec3( 1, 0, 0 );
 normalMatrix[1] = vec3( 0, 1, 0 );
 normalMatrix[2] = vec3( 0, 0, 1 );
 vec3 vLightPosition = vec3(0.5, 0.5, -0.5);

 vVaryingNormal = normalMatrix * vNormal;
 vec4 vPosition4 = mvMatrix * vVertex;
 vec3 vPosition3 = vPosition4.xyz / vPosition4.w;
 vVaryingLightDir = normalize(vLightPosition - vPosition3);
 gl_Position = mvpMatrix * vVertex;
}
