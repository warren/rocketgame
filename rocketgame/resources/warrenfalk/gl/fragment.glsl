#version 330
out vec4 vFragColor;
//uniform vec4 ambientColor;
//uniform vec4 diffuseColor;
//uniform vec4 specularColor;
smooth in vec3 vVaryingNormal;
smooth in vec3 vVaryingLightDir;

void main(void) {
	vec4 ambientColor = vec4(0, 0, 0, 1);
	vec4 diffuseColor = vec4(1, 0, 0, 1);
	vec4 specularColor = vec4(1, 1, 1, 1);

 float diff = max(0.0, dot(normalize(vVaryingNormal), normalize(vVaryingLightDir)));
 vFragColor = diff * diffuseColor;
 vFragColor += ambientColor;
 vec3 vReflection = normalize(reflect(-normalize(vVaryingLightDir),normalize(vVaryingNormal)));
 float spec = max(0.0, dot(normalize(vVaryingNormal), vReflection));

 if(diff != 0) {
   float fSpec = pow(spec, 32.0);
   vFragColor.rgb += vec3(fSpec, fSpec, fSpec);
 }

}